package com.vazhasapp.tbcshemajamebeli2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.vazhasapp.tbcshemajamebeli2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAdd.setOnClickListener {
            addUserInDummyData()
        }

    }

    private fun addUserInDummyData() {
        val firstName = binding.etFirstName.text.toString()
        val lastName = binding.etLastName.text.toString()
        val age = binding.etAge.text.toString().toInt()
        val email = binding.etEmail.text.toString()

        val userDummyData = mutableListOf<Member>()
        userDummyData.add(Member(firstName, lastName,age,email))

    }

}
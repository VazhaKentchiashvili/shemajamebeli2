package com.vazhasapp.tbcshemajamebeli2

data class Member(
    val firstName: String,
    val lastName: String,
    val age: Int,
    val email: String
)
